from django.urls import include, path
from health_care_pro import views

urlpatterns = [
    path('health-care-pros', views.get_health_care_pros, name="get_health_care_pros"),
    path('health-care-pros/<int:idHealthCarePro>', views.get_health_care_pros, name="get_health_care_pros"),
]
