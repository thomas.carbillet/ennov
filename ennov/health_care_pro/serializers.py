from rest_framework import serializers

class HealthCareProfessionalSerializer(serializers.Serializer):
    firstname = serializers.CharField(max_length=254)
    lastname = serializers.CharField(max_length=254)
    email = serializers.EmailField()
    address = serializers.CharField(max_length=254)
    isDeleted = serializers.BooleanField()
