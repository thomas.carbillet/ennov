from health_care_pro.models import HealthCareProfessional
from rest_framework.decorators import api_view
from django.http import JsonResponse
from health_care_pro.serializers import HealthCareProfessionalSerializer


@api_view(['GET'])
def get_health_care_pros(request, idHealthCarePro = 0):
    """
    API endpoint that get all or one health care professional(s)
    that have not been deleted from database
    """
    if idHealthCarePro == 0:
        health_care_pros = HealthCareProfessional.objects.filter(isDeleted = False).values()
        response = HealthCareProfessionalSerializer(health_care_pros, many=True)
        if not health_care_pros:
            return JsonResponse({'message': 'Database is empty'}, status=200)

    else:
        try:
            health_care_pros = HealthCareProfessional.objects.get(isDeleted=False, id=idHealthCarePro)
            response = HealthCareProfessionalSerializer(health_care_pros)
        except HealthCareProfessional.DoesNotExist:
            return JsonResponse({'message': 'Health care professional not found'}, status=404)

    return JsonResponse(response.data, status=200, safe=False)
