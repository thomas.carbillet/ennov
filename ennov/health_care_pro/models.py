from django.db import models

class HealthCareProfessional(models.Model):
    firstname = models.CharField(max_length=254)
    lastname = models.CharField(max_length=254)
    email = models.EmailField(unique=True)
    address = models.CharField(max_length=254)
    isDeleted = models.BooleanField(default=False)
