Health Care Professionals
=========================

## The project
Using Django & Django Rest Framework, give us your thoughts and the way you would develop a small application server that exposes a REST API. This API will provide the ability to create, retrieve, update & delete health care professional objects in the database. A health care professional object should be composed of a first name, a last name, an email address (must be unique) and a postal address. Furthermore, for practical reasons we should never really remove any object from the database. When performing a 'delete' operation, health care professionals objects should be "flagged" as deleted. Then, as if it was really deleted from the database, it should not be possible to retrieve a "deleted" health care professional using the API.

## Setting up the environment
First, we should set up the environment to create a REST API using `django` and `djangorestframework` using the following commands:
```
$ source env/bin/activate
(env)$ pip install django
(env)$ django-admin startproject ennov
(env)$ cd ennov
(env)$ python3 manage.py startapp healt_care_pro
(env)$ pip install djangorestframework
```
We will work in a virtual environment because we don't want to depend on a global version of `django`.


Adding `rest_framework` and `healt_care_pro` to INSTALLED_APPS in settings.py file to finish the set up.
```
INSTALLED_APPS = [
    ...,
    'healt_care_pro',
    'rest_framework',
    ...,
]
```

## Creating the database
We chose Postgres database for this project. The choice is arbitrary.
This project will have one database named `ennov` and one table named `health_care_pro`.
Here is the model that we are going to use:

Column name | Type | Index | Description
--- | --- | --- | ---
id | integer | primary | id of the health care professional
firstname | string (254) | | First name of the health care professional
lastname | string (254) | | Last name of the health care professional
email | string (254) | unique | Email address of the health care professional
address | string (254) / test | | Postal address of the health care professional
isDeleted | boolean | | `false` if the user is not deleted. `true` otherwise

## Endpoints
We could think of 5 endpoints for this project. All responses coming from the server should have a HTTP code (200, 201, 404, 405, ....) to give information about the way requests have been handled.

### First endpoint: Getting all the health care professionals (READ)
This endpoint will return all the health care professionals that are in the database with `isDeleted = false` (`HTTP_CODE = 200`).
```
GET /health-care-pros/
```

### Second endpoint: Getting a single health care professional (READ)
This endpoint will return only one health care professional from the database (`HTTP_CODE = 200`) or return an error message if the health car porfessional could not be found or has `isDeleted = true` (`HTTP_CODE = 404`).
```
GET /health-care-pros/:idHealthCarePro
```

### Third endpoint: Creating a new health care professional (CREATE)
Here we can create a new health care professional in the database (`HTTP_CODE = 201`). We have to be careful that the email address is unique (i.e. not already in database) and that body parameters are not missing (`HTTP_CODE = 400`).
```
POST /health-care-pros/

body: {
  "firstname": <firstname>,
  "lastname": <lastname>,
  "email": <email>,
  "address": <address>
}
```

What should be done if a health care professional has `isDeleted = true` ? Should we update it to `isDeleted = false` ?

### Fourth endpoint: Updating an existing health care professional (UPDATE)
Here we can update an existing health care professional in the database (`HTTP_CODE = 200`). We have to be careful that the email address is unique (i.e. not already in database) and that body parameters are not missing (`HTTP_CODE = 400`).
```
PUT /health-care-pros/:idHealthCarePro

body: {
  "firstname": <updated_firstname>,
  "lastname": <updated_lastname>,
  "email": <updated_email>,
  "address": <updated_address>
}
```

### Fifth endpoint: Deleting a health care professional (DELETE)
We can delete a health care professional from the database (`HTTP_CODE = 200`). If the health care professional is not in the database or has `isDeleted = true`, the server should return an error message (`HTTP_CODE = 404`).
```
DELETE /health-care-pros/:idHealthCarePro
```

## Authorization
Authorization could be handled with a key set in the headers.

## Testing
Each endpoint should be tested separatly. [APIRequestFactory](https://www.django-rest-framework.org/api-guide/testing/) could be used

## Documentation
The documentation could be written using [Swagger](https://django-rest-swagger.readthedocs.io/en/latest/).
To see an exemple of a real documentation I have made using Swagger, please go to <https://walk.billatraining.com/api/doc>.

## Implementation for the first and second endpoints
Here is the implementation for the first and second endpoints using `djangorestframework`.

Model `HealthCareProfessional`
```python
from django.db import models

class HealthCareProfessional(models.Model):
    firstname = models.CharField(max_length=254)
    lastname = models.CharField(max_length=254)
    email = models.EmailField(unique=True)
    address = models.CharField(max_length=254)
    isDeleted = models.BooleanField(default=False)
```

View `get_health_care_pros` that handles both getting all the health care professionals and a single health care professional depending on the path parameter `idHealthCarePro` from the database.
```python
from health_care_pro.models import HealthCareProfessional
from rest_framework.decorators import api_view
from django.http import JsonResponse
from health_care_pro.serializers import HealthCareProfessionalSerializer


@api_view(['GET'])
def get_health_care_pros(request, idHealthCarePro = 0):
    """
    API endpoint that get all or one health care professional(s)
    that have not been deleted from database
    """
    if idHealthCarePro == 0:
        health_care_pros = HealthCareProfessional.objects.filter(isDeleted = False).values()
        response = HealthCareProfessionalSerializer(health_care_pros, many=True)
        if not health_care_pros:
            return JsonResponse({'message': 'Database is empty'}, status=200)

    else:
        try:
            health_care_pros = HealthCareProfessional.objects.get(isDeleted=False, id=idHealthCarePro)
            response = HealthCareProfessionalSerializer(health_care_pros)
        except HealthCareProfessional.DoesNotExist:
            return JsonResponse({'message': 'Health care professional not found'}, status=404)

    return JsonResponse(response.data, status=200, safe=False)
```

 Urls
 ```python
 from django.urls import include, path
 from health_care_pro import views

 urlpatterns = [
     path('health-care-pros', views.get_health_care_pros, name="get_health_care_pros"),
     path('health-care-pros/<int:idHealthCarePro>', views.get_health_care_pros, name="get_health_care_pros"),
 ]
 ```
